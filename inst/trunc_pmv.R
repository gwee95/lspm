load(file = "~/lspm/inst/extdata/adapt/node100results4d3_adapt.Rdata")
load(file = "~/lspm/inst/extdata/adapt/node100results4d3_adapt_low_d.Rdata")
load(file = "~/lspm/inst/extdata/adapt/node100results4d3_adapt_same_d.Rdata")
load(file = "~/lspm/inst/extdata/adapt/node100results4d3_adapt_high_d.Rdata")

source("~/lspm/inst/trunc_network.R")

# open the pdf file
cairo_pdf("~/lspm/inst/extdata/adapt/figure/trunc_pmv.pdf", width = 10, height = 4)

# Setup the plotting area
old.par <- par(no.readonly = TRUE) # save current layout setting
par(mfrow=c(1,4))
layout.matrix <- matrix(1:4, nrow = 1, ncol = 4)
layout(mat = layout.matrix, widths = c(2.6,2,2,2.2))
par(mar = c(5.6, 4.5, 4.1, 0))


# p_0 = auto --------------------------------------------------------------

vioplot(cbind(node100results4d3_adapt[[paste0("seed",(seed_number[6]))]]$mcmc_chain$"8D"$variances[complete.cases(node100results4d3_adapt$seed126055$mcmc_chain$"8D"$variances),,drop=F]),
        ylim=c(0,5), cex=0.2, rectCol = NA, lineCol = NA, cex.lab=1.5, cex.main=1.5, cex.axis=1.5,
        colMed = NA, border=NA, col=rgb(red = 0, green=0, blue=0,alpha=0),
        #main='Posterior Mean Variance vs Dimension',
        xlab= "", ylab="")
title(main=expression(paste(p[0]," automatically chosen")), line=2.3, cex.main=1.5)
title(main=expression(paste("(", p[0], " = auto)")), line=1, cex.main=1.5)

for(seed in seed_number) {
  lapply(node100results4d3_adapt[[paste0("seed",(seed))]]$mcmc_chain, function(x) vioplot(x$variances[complete.cases(x$variances),,drop=F], add=T, cex=0.2, rectCol = NA, lineCol = NA, cex.lab=1.5, cex.main=1.5, cex.axis=1.5,
                                                                                           colMed = NA, border=NA, col=rgb(red = 0, green=0, blue=0,alpha=0.03)))
}
title(xlab=expression(paste("Dimension \u2113")), cex.lab=1.5, line=2.5)
title(ylab=expression(paste("Variance ", omega["\u2113"]^{-1})), line=2.2, cex.lab=1.75)
# for(seed in seed_number[2:15]) {
#   vioplot(node100results4d3_adapt[[paste0("seed",(seed))]]$mcmc_chain$"4D"$variances, add=TRUE, cex=0.2, rectCol = NA, lineCol = NA,
#           colMed = NA, border=NA, col=rgb(red = 0, green=0, blue=0,alpha=0.03))
# }
points(1/cumprod(c(0.5,1.1,1.05,1.15)), col='red', pch=4, cex=2, lwd=3) # true
# points(apply(t(as.data.frame(lapply(node20results_thin,function(x) apply(x$variances,2,mean)))),2,mean),pch=20, cex=2, col="green")

legend("topright", legend=c('Posterior distribution', "True value"), pch=c(20,4), col=c("black","red"), cex=1.2)


# p_0 = 2 --------------------------------------------------------------

par(mar = c(5.6, 0, 4.1, 0)) # left margin set as 0

vioplot(cbind(node100results4d3_adapt[[paste0("seed",(seed_number[6]))]]$mcmc_chain$"8D"$variances[complete.cases(node100results4d3_adapt$seed126055$mcmc_chain$"8D"$variances),,drop=F]),
        ylim=c(0,5), cex=0.2, rectCol = NA, lineCol = NA, cex.lab=1.5, cex.main=1.5, cex.axis=1.5,
        colMed = NA, border=NA, col=rgb(red = 0, green=0, blue=0,alpha=0),
        #main='Posterior Mean Variance vs Dimension',
        xlab= "", ylab="", yaxt="n")
title(main=expression(paste(p[0]," lower than the truth")), line=2.3, cex.main=1.5)
title(main=expression(paste("(", p[0], " = 2)")), line=1, cex.main=1.5)

for(seed in seed_number) {
  lapply(node100results4d3_adapt_low_d[[paste0("seed",(seed))]]$mcmc_chain, function(x) vioplot(x$variances[complete.cases(x$variances),,drop=F], add=T, cex=0.2, rectCol = NA, lineCol = NA, cex.lab=1.5, cex.main=1.5, cex.axis=1.5,
                                                                                                colMed = NA, border=NA, col=rgb(red = 0, green=0, blue=0,alpha=0.03)))
}
axis(1, at=1:8, las=1, labels=1:8, cex.axis=1.5)

title(xlab=expression(paste("Dimension \u2113")), cex.lab=1.5, line=2.5)
title(ylab=expression(paste("Variance ", omega["\u2113"]^{-1})), line=2.2)
points(1/cumprod(c(0.5,1.1,1.05,1.15)), col='red', pch=4, cex=2, lwd=3) # true
# points(apply(t(as.data.frame(lapply(node50results_thin,function(x) apply(x$variances,2,mean)))),2,mean),pch=20, cex=2, col="green")


# p_0 = 4 --------------------------------------------------------------


vioplot(cbind(node100results4d3_adapt[[paste0("seed",(seed_number[6]))]]$mcmc_chain$"8D"$variances[complete.cases(node100results4d3_adapt$seed126055$mcmc_chain$"8D"$variances),,drop=F]),
        ylim=c(0,5), cex=0.2, rectCol = NA, lineCol = NA, cex.lab=1.5, cex.main=1.5, cex.axis=1.5,
        colMed = NA, border=NA, col=rgb(red = 0, green=0, blue=0,alpha=0),
        #main='Posterior Mean Variance vs Dimension',
        xlab= "", ylab="", yaxt="n")
title(main=expression(paste(p[0]," same as the truth")), line=2.3, cex.main=1.5)
title(main=expression(paste("(", p[0], " = 4)")), line=1, cex.main=1.5)

for(seed in seed_number) {
  lapply(node100results4d3_adapt_same_d[[paste0("seed",(seed))]]$mcmc_chain, function(x) vioplot(x$variances[complete.cases(x$variances),,drop=F], add=T, cex=0.2, rectCol = NA, lineCol = NA, cex.lab=1.5, cex.main=1.5, cex.axis=1.5,
                                                                                                 colMed = NA, border=NA, col=rgb(red = 0, green=0, blue=0,alpha=0.03)))
}
title(xlab=expression(paste("Dimension \u2113")), cex.lab=1.5, line=2.5)
title(ylab=expression(paste("Variance ", omega["\u2113"]^{-1})), line=2.2)
axis(1, at=1:8, las=1, labels=1:8, cex.axis=1.5)

points(1/cumprod(c(0.5,1.1,1.05,1.15)), col='red', pch=4, cex=2, lwd=3) # true
# points(apply(t(as.data.frame(lapply(node100results_thin,function(x) apply(x$variances,2,mean)))),2,mean),pch=20, cex=2, col="green")


# p_0 = 10 --------------------------------------------------------------

par(mar = c(5.6, 0, 4.1, 0.25)) # right margin set as 0.1

vioplot(cbind(node100results4d3_adapt[[paste0("seed",(seed_number[6]))]]$mcmc_chain$"8D"$variances[complete.cases(node100results4d3_adapt$seed126055$mcmc_chain$"8D"$variances),,drop=F]),
        ylim=c(0,5), cex=0.2, rectCol = NA, lineCol = NA, cex.lab=1.5, cex.main=1.5, cex.axis=1.5,
        colMed = NA, border=NA, col=rgb(red = 0, green=0, blue=0,alpha=0),
        #main='Posterior Mean Variance vs Dimension',
        xlab= "", ylab="", yaxt="n")
title(main=expression(paste(p[0]," higher than the truth")), line=2.3, cex.main=1.5)
title(main=expression(paste("(", p[0], " = 10)")), line=1, cex.main=1.5)

for(seed in seed_number) {
  lapply(node100results4d3_adapt_high_d[[paste0("seed",(seed))]]$mcmc_chain, function(x) vioplot(x$variances[complete.cases(x$variances),,drop=F], add=T, cex=0.2, rectCol = NA, lineCol = NA, cex.lab=1.5, cex.main=1.5, cex.axis=1.5,
                                                                                                 colMed = NA, border=NA, col=rgb(red = 0, green=0, blue=0,alpha=0.03)))
}

title(xlab=expression(paste("Dimension \u2113")), cex.lab=1.5, line=2.5)
title(ylab=expression(paste("Variance ", omega["\u2113"]^{-1})), line=2.2)
axis(1, at=1:8, las=1, labels=1:8, cex.axis=1.5)
points(1/cumprod(c(0.5,1.1,1.05,1.15)), col='red', pch=4, cex=2, lwd=3) # true
# points(apply(t(as.data.frame(lapply(node200results_thin,function(x) apply(x$variances,2,mean)))),2,mean),pch=20, cex=2, col="green")


par(old.par) # restore previous plot layout setting

# Close the pdf file
dev.off()

#10.57 x 4.83
